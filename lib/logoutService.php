<?php
session_start();
unset($_SESSION['login']);
unset($_SESSION['id']);
session_destroy();
?>
<html lang="de">
<meta http-equiv="refresh" content="5;url=../index.html">
<head>
    <title>R24 Logout</title>
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
</head>
<body>
<div class="mb-3 mt-5 container text-center alert alert-info">
    <h1>Vielen Dank, dass du Kunde bei R24 bist!</h1>
    <h2>Du wirst abgemeldet ...</h2>
    <br><br><p>Falls die automatische Weiterleitung nicht funktioniert, klicke bitte <a href="../index.html" title="Startseite R24">hier</a></p>
</div>
</body>
</html>
<?php
header("Refresh:5, url=../index.html");
exit();
?>



