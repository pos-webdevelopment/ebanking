<?php
session_start();
require_once "../models/User.php";
require_once "../models/Account.php";
require_once "../models/AccountTypes.php";
require_once "../models/Transaction.php";
//error_reporting(0);
if (!$_SESSION['service']) {
    unset($_SESSION['id']);
    unset($_SESSION['login']);
    session_destroy();
    header("Location: ../index.html");
}
$id = $_GET['id'];
$ID = $_SESSION['id'];
$account = Account::getAccountData($id);
$userId = $account->getUID();
$date = date('Y-m-d H:i:s');
if (isset($_POST['submit'])) {
    if (isset($_POST['add'])) {
        Transaction::executeTransaction($userId, $ID, $date, "Bareinzahlung durch R24", $_POST['amount']);
        Account::addBalanceToPayee($_POST['amount'], $userId);
    } else {
        Transaction::executeTransaction($ID, $userId, $date, "Barauszahlung durch R24", $_POST['amount']);
        Account::reduceBalanceFromPayer($_POST['amount'], $userId);
    }
}
$accountTypes = AccountTypes::getAllAccounts();
$account = Account::getAccountData($id);
$userId = $account->getUID();
$u = User::getUserById($userId);
?>
<html lang="de">
<head>
    <title>R24 SERVICE</title>
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="wrapper">
        <div class="mt-5 container alert alert-info text-center">
            <h1>SERVICE-BEREICH</h1>
            <h2>Barauszahlungen und -einzahlungen</h2>
        </div>
        <br>
        <div class="float-right">
            <a href="r24ServicePage.php">[ Zurück ]</a>
        </div>
        <br><br>
    </div>
    <h3>Kontodaten</h3>
    <table class="table table-responsive-sm table-striped">
        <tr>
            <th>Vorname</th>
            <td><?= $account->getUFirstName() ?></td>
        </tr>
        <tr>
            <th>Nachname</th>
            <td><?= $account->getULastName() ?></td>
        </tr>
        <tr>
            <th>E-Mail</th>
            <td><?= $account->getUMail() ?></td>
        </tr>
        <tr>
            <th>Wohnort</th>
            <td><?= $u->getUOrt() ?></td>
        </tr>
        <tr>
            <th>Postleitzahl</th>
            <td><?= $u->getUZIP() ?></td>
        </tr>
        <tr>
            <th>Adresse</th>
            <td><?= $u->getUStrasse() ?></td>
        </tr>
        <tr>
            <th>Land</th>
            <td><?= $u->getULand() ?></td>
        </tr>
        <tr>
            <th>Status</th>
            <td><?= $u->getUStatus() ?></td>
        </tr>
        <tr>
            <th>Kontotyp</th>
            <td><?= $account->getTName() ?></td>
        </tr>
        <tr>
            <th>Kontostand</th>
            <td>EUR <?= $account->getABalance() ?></td>
        </tr>
        <tr>
            <th>IBAN</th>
            <td><?= $account->getAIBAN() ?></td>
        </tr>
    </table>
    <br><br>
    <h3>Transaktionen</h3>
    <table class="table">
        <thead class="text-info">
        <th>Empfänger</th>
        <th>Absender</th>
        <th>Valuta</th>
        <th>Verwendungszweck</th>
        <th class="text-right">Betrag</th>
        </thead>
        <tbody>
        <?php
        $count = sizeof(Transaction::getTransactionData($userId));
        for ($i = 0; $i < $count; $i++) {
            $t = Transaction::getTransactionData($userId)[$i];
            echo "<tr><td>" . $t->Payee . "</td>";
            echo "<td>" . $t->Payer . "</td>";
            echo "<td>" . date("d.m.Y H:i:s", strtotime($t->getTDate())) . "</td>";
            echo "<td>" . $t->getTReference() . "</td>";
            echo "<td class='text-right'>€ " . $t->getTAmount() . "</td></tr>";
        }
        ?>
        </tbody>
    </table>
    <br><br>
    <h3>Ein- und Auszahlungen</h3>
    <div class="col-sm-12 row">
        <div class="col-sm-5 border border-success">
            <form action="r24DetailsPage.php?id=<?= $id ?>" method="post">
                <div class="form-group">
                    <label for="amount" class="form-inline">€</label>
                    <input type="number" step=".01" min="0.01" id="amount" name="amount" required/>
                    <input hidden name="add">
                </div>
                <button type="submit" name="submit" class="btn btn-outline-success">Bareinzahlung</button>
            </form>
        </div>

        <div class="col-sm-2"></div>

        <div class="col-sm-5 border border-danger">
            <form action="r24DetailsPage.php?id=<?= $id ?>" method="post">
                <div class="form-group">
                    <label for="amount" class="form-inline">€</label>
                    <input type="number" step=".01" min="0.01" id="amount" name="amount" required/>
                    <input hidden name="reduce">
                </div>
                <button type="submit" name="submit" class="btn btn-outline-danger">Barauszahlung</button>
            </form>
        </div>
    </div>
    <div class="mt-5 text-center">
        <button class="btn btn-info" onClick="window.print()">Seite ausdrucken</button>
    </div>

</div>
<br><br>
</body>
<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-left">Copyright © R24 Onlinebank 2020</div>
            <div class="col-lg-4 text-lg-right">
                <a class="mr-3" href="#!">Privacy Policy</a>
                <a href="#!">Terms of Use</a>
            </div>
        </div>
    </div>
</footer>
</html>
