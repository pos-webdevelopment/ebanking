INSERT INTO tbl_user(uVorname, uNachname, uMail, uPass) VALUES('Max', 'Mustermann', 'max@mustermann.com', sha('1234'));
INSERT INTO tbl_user(uVorname, uNachname, uMail, uPass) VALUES('Maria', 'Musterfrau', 'maria@musterfrau.com', sha('5678'));
INSERT INTO tbl_user(uVorname, uNachname, uMail, uPass) VALUES('Franz', 'Sepp', 'franz@sepp.com', sha('abcd'));
INSERT INTO tbl_user(uVorname, uNachname, uMail, uPass) VALUES('John', 'Doe', 'john.doe@mail.com', sha('johndoe'));

INSERT INTO tbl_account_types(tName) VALUES('Girokonto');
INSERT INTO tbl_account_types(tName) VALUES('Sparkonto');
INSERT INTO tbl_account_types(tName) VALUES('Bausparer');
INSERT INTO tbl_account_types(tName) VALUES('Aktienhandel');

INSERT INTO tbl_account(aBalance, tID, uID) VALUES (10000.00, 4, 1);
INSERT INTO tbl_account(aBalance, tID, uID) VALUES (10000.00, 3, 2);
INSERT INTO tbl_account(aBalance, tID, uID) VALUES (10000.00, 2, 3);
INSERT INTO tbl_account(aBalance, tID, uID) VALUES (10000.00, 1, 4);

INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tAmount, tSuccess) VALUES(1,2,'2020-10-20', 500, true);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tAmount, tSuccess) VALUES(1,4,'2020-10-20', 500, true);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tAmount, tSuccess) VALUES(1,2,'2020-10-20', 500, true);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tAmount, tSuccess) VALUES(1,2,'2020-10-20', 500, true);