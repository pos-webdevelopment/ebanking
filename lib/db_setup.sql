drop database if exists ebanking;
create database ebanking;
USE ebanking;

CREATE TABLE if not EXISTS tbl_user
(
    uID         INTEGER      NOT NULL AUTO_INCREMENT,
    uFirstName  varchar(255) NOT NULL,
    uLastName   varchar(255) NOT NULL,
    uMail       varchar(255) NOT NULL,
    uPass       varchar(255) NOT NULL,
    uStrasse    varchar(255) NULL,
    uOrt        varchar(255) NULL,
    uLand       varchar(255) NULL,
    uZIP        int(5)       NULL,
    uStatus     varchar(255) NULL,
    uProfilePic BLOB         NULL,
    unique (uFirstName, uLastName, uMail),
    PRIMARY KEY (uID)
);

-- Service Accounts
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES('James', 'Wyatt, R24', 'service@r24.at', sha1('r24'));
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES('Robert', 'Spiegl, R24', 'technik@r24.at', sha1('r24'));

CREATE TABLE if not EXISTS tbl_account_types
(
    tID             INTEGER      NOT NULL AUTO_INCREMENT,
    tName           varchar(255) NOT NULL,
    tRate           FLOAT        NULL, #Rate, zB bei Bausparer
    tDebitInterest  FLOAT        NULL, #Sollzinsen
    tCreditInterest FLOAT        NULL, #Habenzinsen
    UNIQUE (tName, tRate, tDebitInterest, tCreditInterest),
    PRIMARY KEY (tID)
);

-- default accounts provided by R24
INSERT INTO tbl_account_types(tName, tDebitInterest, tCreditInterest) VALUES('Girokonto', 0.1358, 1.057);
INSERT INTO tbl_account_types(tName, tDebitInterest, tCreditinterest) VALUES('Sparkonto', 0.207, 1.119);
INSERT INTO tbl_account_types(tName, tRate, tDebitInterest, tCreditInterest) VALUES('Bausparer', 750, 2.40, 1.39);
INSERT INTO tbl_account_types(tName, tDebitInterest, tCreditinterest) VALUES('Aktienhandel', 0.015, 1.003);

CREATE TABLE if not EXISTS tbl_account
(
    aID      INTEGER     NOT NULL AUTO_INCREMENT,
    aIBAN    varchar(20) NULL,     # example-iban: AT900002400000000815 (2xchar, 1x'24', 16xdigit)
    aBIC     varchar(11) NULL,     # example-bic: NTSBDEB1XXX (8xchar, 3xdigit)
    aBalance DOUBLE      NOT NULL,
    tType    INTEGER     NOT NULL, # Account Type
    uID      INTEGER     NOT NULL, # User-ID
    UNIQUE (tType, uID),
    PRIMARY KEY (aID),
    FOREIGN KEY (uID) REFERENCES tbl_user (uID) ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (tType) REFERENCES tbl_account_types (tID) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE if not EXISTS tbl_transaction
(
    tID        INTEGER      NOT NULL AUTO_INCREMENT,
    uPayee     INTEGER      NOT NULL, #Zahlungsempfänger, Begünstigter
    uPayer     INTEGER      NOT NULL, #Einzahler, Belasteter
    tDate      DATETIME     NOT NULL,
    tReference varchar(255) NOT NULL,
    tAmount    DOUBLE       NOT NULL,
    tSuccess   BOOLEAN      NULL,
    UNIQUE (tID, uPayee, uPayer, tDate),
    PRIMARY KEY (tID),
    FOREIGN KEY (uPayee) REFERENCES tbl_user (uID) ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (uPayer) REFERENCES tbl_user (uID) ON UPDATE CASCADE ON DELETE RESTRICT
);
