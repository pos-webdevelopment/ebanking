<!--
=========================================================
* Paper Dashboard 2 - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-2
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->

<?php
session_start();
require_once "../models/User.php";
require_once "../models/Account.php";
require_once "../models/Transaction.php";
error_reporting(0);
$message = null;
if (!isset($_SESSION['login']) && isset($_SESSION['id']) || !$_SESSION['login']) {
    header("Location: ../index.html");
    exit();
} else {
    $ID = $_SESSION['id'];
}
if (isset($_POST['commit'])) {
    if (isset($_POST['payee']) && isset($_POST['iban']) && isset($_POST['amount']) && isset($_POST['reference'])) {
        $date = date('Y-m-d H:i:s');
        if (Transaction::commit($_POST['payee'], $ID, $_POST['reference'], $_POST['iban'], $_POST['amount'], $date)) {
            $message = "Überweisung hat geklappt :)";
        } else {
            $message = "Fehler! Die eingegebenen Daten sind falsch!";
        }
    } else {
        $message = "Überweisung konnte nicht durchgeführt werden!";
    }
}
$u = User::getUserById($ID);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>R24 - Überweisung durchführen</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>

<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="warning">
        <div class="logo">
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-mini">
                <div class="logo-image-small">
                    <img src="../assets/img/login.png" alt="keyring">
                </div>
            </a>
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-normal">Deine Bank.</a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="../lib/dashboard.php">
                        <i class="nc-icon nc-bank"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="profile.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>Mein R24-Account</p>
                    </a>
                </li>
                <li>
                    <a href="sales.php">
                        <i class="nc-icon nc-tile-56"></i>
                        <p>Umsätze</p>
                    </a>
                </li>
                <li class="active ">
                    <a href="transaktion.php">
                        <i class="nc-icon nc-bell-55"></i>
                        <p>Transaktionen</p>
                    </a>
                </li>
                <li>
                    <a href="../lib/upgrade.php">
                        <i class="nc-icon nc-spaceship"></i>
                        <p>Upgrade to PRO R24</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="javascript:">Überweisung durchführen</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <form>
                        <div class="input-group no-border">
                            <?php echo $u->getULastName(). ", " . $u->getUFirstName() ?>
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="nc-icon nc-bookmark-2"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Navigation</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="dashboard.php">Dashboard</a>
                                <a class="dropdown-item" href="transaktion.php">Transaktionen</a>
                                <a class="dropdown-item" href="sales.php">Umsätze</a>
                                <a class="dropdown-item" href="logoutService.php">Logout</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn-rotate" href="logoutService.php">
                                <i class="nc-icon nc-button-power"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="card-header">
                                <h5 class="card-title">Transaktionsdaten</h5>
                            </div>
                            <div class="card-body">
                                <form action="transaktion.php" method="post">
                                    <div class="row">
                                        <div class="col-md-5 pr-1">
                                            <div class="form-group">
                                                <label>Deine Bank</label>
                                                <input type="text" class="form-control" disabled=""
                                                       placeholder="Company" value="R24 - Deine Onlinebank">
                                            </div>
                                        </div>
                                        <div class="col-md-5 pl-1">
                                            <div class="form-group">
                                                <label>Dein IBAN</label>
                                                <input type="text" class="form-control" disabled
                                                       placeholder="<?= Account::getAccountData($ID)->getAIBAN() ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Name des Empfängers/Begünstigten</label>
                                                <input type="text" id="payee" name="payee" class="form-control"
                                                       placeholder="Bitte fülle dieses Feld aus!">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>IBAN des Empfängerse</label>
                                                <input type="text" id="iban" name="iban" class="form-control"
                                                       placeholder="Bitte fülle dieses Feld aus!">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="name">Betrag in EUR</label>
                                                <input type="number" step=".01" min="0.01" id="amount" name="amount"
                                                       class="form-control" placeholder="Bitte fülle dieses Feld aus!">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Verwendungszweck / Referenz</label>
                                                <input type="text" id="reference" name="reference" class="form-control"
                                                       placeholder="Bitte fülle dieses Feld aus!">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="container text-center">
                                            <button type="submit" id="commit" name="commit"
                                                    class="btn btn-success btn-round">Überweisung durchführen
                                            </button>
                                            <a href="transaktion.php" class="btn btn-danger btn-round">Abbrechen</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        if ($message != null) {
                                            ?>
                                            <div class="col-md-12 alert alert-info text-dark text-center">
                                                <p><?= $message ?></p>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>