<!-- This templates was made by Colorlib (https://colorlib.com) -->
<!-- Edited -->
<?php
session_start();
$_SESSION['login'] = false;
require_once "../models/User.php";
if(isset($_POST['signin'])) {
    if(User::login($_POST['mail'], $_POST['pass'])) {
        $_SESSION['login'] = true;
        $user = User::getUserByMail($_POST['mail']);
        $_SESSION['id'] = $user->getUId();
        if(strpos($_POST['mail'], "r24")) {
            $_SESSION['service'] = true;
            $user = User::getUserByMail($_POST['mail']);
            $_SESSION['id'] = $user->getUId();
            header("Location: r24ServicePage.php");
            exit();
        } else {
            header("Location: dashboard.php");
            exit();
        }
    } else {
        $_SESSION['login'] = false;
        unset($_SESSION['id']);
       ?>
        <div class="alert alert-danger text-center">Falsche Anmeldedaten. Bitte versuche es erneut!</div>
<?php
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>R24 Login</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
</head>
<body>

<!-- Sign in  Form -->
<section class="sign-in">
    <div class="container">
        <div class="signin-content">
            <div class="signin-image">
                <a href="register.php" class="signup-image-link">
                <figure><img src="../assets/img/Add_user_icon_blue.svg.png" alt="sign up image"></figure>
                Noch nicht dabei? Jetzt neuen Account anlegen!</a>
            </div>
            <div class="signin-form">
                <h2 class="form-title">Sign up</h2>
                <form action="login.php" method="post" class="register-form" id="login-form">
                    <div class="form-group">
                        <label for="mail"><i class="zmdi zmdi-account material-icons-name"></i></label>
                        <input type="text" name="mail" id="mail" placeholder="Deine E-Mail-Adresse" required/>
                    </div>
                    <div class="form-group">
                        <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                        <input type="password" name="pass" id="pass" placeholder="Dein Passwort" required/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                        <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                    </div>
                    <div class="form-group form-button">
                        <input type="submit" name="signin" id="signin" class="form-submit" value="Einloggen"/>
                    </div>
                </form>
                <div class="social-login">
                    <span class="social-label">Or login with</span>
                    <ul class="socials">
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <a class="btn btn-primary" href="../index.html#login">Zurück zu R24</a>
    </div>
</section>


</body>
</html>