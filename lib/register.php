<!-- This templates was made by Colorlib (https://colorlib.com) -->
<!-- Edited -->

<?php
session_start();
require_once "../models/AccountTypes.php";
require_once "../models/Account.php";
include_once "../models/User.php";
$_SESSION['login'] = false;
error_reporting(0);
if (isset($_POST['register'])) {
    if (isset($_POST['pass']) && isset($_POST['re_pass'])) {
        if (strcmp($_POST['pass'], $_POST['re_pass']) == 0) {
            if (isset($_POST['name'])) {
                $fullName = $_POST['name'];
                $split = explode(" ", $fullName);
                $firstName = $split[0];
                $lastName = $split[1];
                if ($split[0] != null && $split[1] != null && $split[2] == null) {
                    if (!strpos($_POST['mail'], 'r24') && User::register($firstName, $lastName, $_POST['mail'], $_POST['pass'])) {
                        $user = User::getUserByMail($_POST['mail']);
                        $userId = $user->getUId();
                        echo "UserID: " . $userId;
                        $iban = "AT24" . rand(0,9999999999999999);
                        if(Account::createAccount(100, $_POST['kontotyp'], $iban, $userId)) {
                            $_SESSION['login'] = true;
                            $_SESSION['id'] = $userId;
                            header("Location: dashboard.php");
                            exit();
                        } else {
                            echo "Konto konnte nicht erstellt werden!";
                            echo "Konto-ID: ". $_POST['kontotyp'];
                            echo "User-ID: " . $userId['uId'];
                            var_dump($userId);
                        }
                    } else {
                        $_SESSION['login'] = false;
                        ?>
                        <div class="alert alert-danger text-center">Username und E-Mail-Adresse werden bereits
                            verwendet!<br>Bitte melde Dich an oder verwende andere Daten.
                        </div>
                        <?php
                    }
                } else {
                    if ($split[2] != null) {
                        ?>
                        <div class="alert alert-danger text-center">Bitte nur Vorname und Nachname, durch Leerzeichen
                            getrennt, angeben!
                        </div>
                        <?php
                    }
                }
            }
        } else {
            ?>
            <div class="alert alert-danger text-center">Die angegebenen Passwörter stimmen nicht überein!
            </div>
            <?php
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bei R24 registrieren</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
</head>
<body>
<div class="container">
    <div class="signup-content">
        <div class="signup-form">
            <h2 class="form-title">Neukunden-Registrierung</h2>
            <form method="post" class="register" id="register" action="register.php">
                <div class="form-group">
                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                    <input type="text" name="name" id="name" placeholder="Vorname Nachname" required/>
                </div>
                <div class="form-group">
                    <label for="mail"><i class="zmdi zmdi-email"></i></label>
                    <input type="email" name="mail" id="mail" placeholder="example@mail.com" required/>
                </div>
                <div class="form-group">
                    <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                    <input type="password" name="pass" id="pass" min="4" max="64" placeholder="Bitte sicheres Passwort wählen!"
                           required/>
                </div>
                <div class="form-group">
                    <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                    <input type="password" name="re_pass" min="4" max="64" id="re_pass"
                           placeholder="Bitte sicheres Passwort wählen!" required/>
                </div>
                <div class="form-group">
                    <label for="kontotyp"><i class="zmdi zmdi-balance"></i></label>
                    <select id="kontotyp" name="kontotyp" class="custom-select" style="border: none; margin-left: 10px">
                        <option selected disabled> --- Bitte wähle Dein Konto</option>
                        <?php
                        $accountTypes = AccountTypes::getAllAccounts();
                        foreach ($accountTypes as $a) {
                            echo "<option value='" . $a->getTID() . "'>" . $a->getTName() . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group alert alert-info">
                    <input type="checkbox" name="agree-term" id="agree-term" required/>
                    <label for="agree-term" class="label-agree-term checkbox"><span><span></span></span>* Ich stimme den
                        <a href="#" class="term-service">Terms of service</a> zu.</label>
                    <input type="checkbox" name="agree-balance" id="agree-balance" required/>
                    <label for="agree-balance" class="label-agree-term checkbox-radios"><span><span></span></span>* Mit
                        der Erstellung des Kontos werden EUR 100.00 gutgeschrieben (siehe AGB).</label>
                </div>
                <div class="form-group form-button">
                    <input type="submit" name="register" id="register" class="form-submit" value="Registrieren"/>
                </div>
            </form>
        </div>
        <div class="signup-image">
            <a href="login.php" class="signup-image-link">
                <figure><img src="../assets/img/login.png" alt="sign up image"></figure>
                Ich bin bereits Kunde!</a>
        </div>
    </div>
    <a class="btn btn-primary" href="../index.html#login">Zurück zu R24</a>
</div>
</body>
</html>