<html lang="de">
<head>
    <title>R24 Install Database</title>
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
</head>
<body>
<div class="mb-3 mt-5 container text-center alert alert-info">
    <h1>Installation der Datenbank durchführen</h1>
    <h4 class="text-danger">Achtung! Dies darf nur von Administratoren durchgeführt werden!</h4>
    <h4 class="text-dark">Wenden Sie sich ggf. bitte an Ihren Bankberater!</h4>
    <br>
    <div class="text-center">
        <a href="../index.html" class="btn btn-success">Zur Homepage</a>
    </div>
    <br>
    <br>
    <form action="installDB.php" method="post">
        <button class="btn btn-outline-secondary" name="file" value="db_setup.sql">Grundlegende Installation</button>
        <button class="btn btn-outline-dark" name="file" value="db_inserts.sql">Mockdaten integrieren</button>
        <a class="btn btn-outline-info" href="../index.html">Zurück</a>
    </form>
</div>
</body>
</html>