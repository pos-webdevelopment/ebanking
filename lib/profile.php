<!--
=========================================================
* Paper Dashboard 2 - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-2
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">
<?php
session_start();
require_once "../models/User.php";
require_once "../models/Account.php";
$message = '';
if (!isset($_SESSION['login']) && isset($_SESSION['id']) || !$_SESSION['login']) {
    header("Location: ../index.html");
    exit();
} else {
    $ID = $_SESSION['id'];
}
if (isset($_POST['submit'])) {
    if (isset($_POST['mail']) && isset($_POST['firstname']) && isset($_POST['lastname'])) { // zusätzliche Prüfung, ob die mindest. erforderlichen Werte angegeben wurden.
        if (!User::updateUserData($ID, $_POST['firstname'], $_POST['lastname'], $_POST['mail'], $_POST['address'], $_POST['city'], $_POST['country'], $_POST['zip'], $_POST['status'])) {
            $message = 'Deine Änderungen wurden erfolgreich gespeichert.';
        }
    }
}
$u = User::getUserById($ID);
$a = Account::getAccountData($ID);
?>
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>R24 - Dein Account</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet"/>
</head>
<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="warning">
        <div class="logo">
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-mini">
                <div class="logo-image-small">
                    <img src="../assets/img/login.png" alt="keyring">
                </div>
            </a>
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-normal">Deine Bank.</a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="../lib/dashboard.php">
                        <i class="nc-icon nc-bank"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active ">
                    <a href="profile.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>Mein R24-Account</p>
                    </a>
                </li>
                <li>
                    <a href="sales.php">
                        <i class="nc-icon nc-tile-56"></i>
                        <p>Umsätze</p>
                    </a>
                </li>
                <li>
                    <a href="transaktion.php">
                        <i class="nc-icon nc-bell-55"></i>
                        <p>Transaktionen</p>
                    </a>
                </li>
                <li class="active-pro">
                    <a href="../lib/upgrade.php">
                        <i class="nc-icon nc-spaceship"></i>
                        <p>Upgrade to PRO R24</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="javascript:">Dein R24-Account</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <form>
                        <div class="input-group no-border">
                            <?php echo $u->getULastName(). ", " . $u->getUFirstName() ?>
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="nc-icon nc-bookmark-2"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Some Actions</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="dashboard.php">Dashboard</a>
                                <a class="dropdown-item" href="transaktion.php">Umsätze</a>
                                <a class="dropdown-item" href="logoutService.php">Logout</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn-rotate" href="logoutService.php">
                                <i class="nc-icon nc-button-power"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="../assets/img/bg.png" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="../assets/img/default-avatar.png" alt="...">
                                    <h5 class="title"><?php echo $u->getUFirstname() . ' ' . $u->getULastName(); ?></h5>
                                </a>
                                <p class="description">
                                    <?php echo $u->getUMail() ?>
                                </p>
                            </div>
                            <p class="description text-center">
                                <?php echo $u->getUStatus(); ?>
                            </p>
                        </div>
                        <div class="card-footer">
                            <h4 class="text-dark text-center">
                                <?php echo $a->getTName() ?>
                            </h4>
                            <hr>
                            <div class="button-container">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-6 ml-auto text-info">
                                        <h5><?= $a->getABalance() ?>
                                            €<br><small>Guthaben</small></h5>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto text-success">
                                        <h5><?= $a->getTCreditInterest() ?>%<br><small>Habenzinsen</small>
                                        </h5>
                                    </div>
                                    <div class="col-lg-3 mr-auto text-danger">
                                        <h5><?= $a->getTDebitInterest() ?>%<br><small>Sollzinsen</small>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="card card-user">
                        <div class="card-header">
                            <h5 class="card-title">Deine Daten bearbeiten</h5>
                        </div>
                        <div class="card-body">
                            <form action="profile.php" method="post" name="user-data">
                                <div class="row">
                                    <div class="col-md-5 pr-1">
                                        <div class="form-group">
                                            <label for="bank">Bank</label>
                                            <input type="text" class="form-control" id="bank" disabled
                                                   placeholder="Company" value="R24 - Deine Onlinebank">
                                        </div>
                                    </div>
                                    <div class="col-md-3 px-1">
                                        <div class="form-group">
                                            <label>IBAN</label>
                                            <input type="text" id="user" name="user" class="form-control" disabled
                                                   value="<?= $a->getAIBAN()  ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label for="mail">Email address *</label>
                                            <input type="email" name="mail" id="mail" class="form-control"
                                                   placeholder="Email" value="<?= $u->getUMail(); ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label for="firstname">First Name *</label>
                                            <input type="text" name="firstname" id="firstname" class="form-control"
                                                   placeholder="First Name" value="<?= $u->getUFirstName(); ?>"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label for="lastname">Last Name *</label>
                                            <input type="text" name="lastname" id="lastname" class="form-control"
                                                   placeholder="Last Name" value="<?= $u->getULastName(); ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <input type="text" name="address" id="address" class="form-control"
                                                   placeholder="Home Address" value="<?= $u->getUStrasse(); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group">
                                            <label for="zip">Postal Code</label>
                                            <input type="number" name="zip" id="zip" class="form-control"
                                                   placeholder="ZIP Code" value="<?= $u->getUZIP(); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" name="city" id="city" class="form-control"
                                                   placeholder="City" value="<?= $u->getUOrt(); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <input type="text" name="country" id="country" class="form-control"
                                                   placeholder="Country" value="<?= $u->getULand(); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">About Me</label>
                                            <textarea class="form-control textarea" id="status" name="status"
                                                      placeholder="Dein Status"><?= $u->getUStatus(); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" id="submit" name="submit"
                                                class="btn btn-success btn-round">Speichern
                                        </button>
                                        <button type="reset" id="reset" name="reset" class="btn btn-danger btn-round">
                                            <a href="profile.php" class="text-white">Abbrechen</a>
                                        </button>
                                    </div>
                                </div>
                                <?php
                                if ($message!='') {
                                    ?>
                                    <div class="row align-content-center text-center">
                                        <div class="alert alert-success text-dark ml-auto mr-auto">
                                            <p><?= $message ?></p>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>