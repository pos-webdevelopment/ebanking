INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass, uStrasse, uOrt, uLand, uZIP, uStatus) VALUES 
('Maximilian', 'Kohler', 'kohler@cern.ch', sha1('1234'), 'Hauptstrasse 1', 'Bern', 'Schweiz', 3000, 'Forschung toppt alles.');
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass, uStrasse, uOrt, uLand, uZIP, uStatus) VALUES 
('Max', 'Mustermann', 'max.muster@mail.com', sha1('1234'), 'Bahnhofstrasse 17', 'Innsbruck', 'Austria', 6020, 'Das Genie beherrscht das Chao.');
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES('Maria', 'Musterfrau', 'maria@musterfrau.com', sha1('5678'));
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES('Franz', 'Sepp', 'franz@sepp.com', sha1('abcd'));
INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES('John', 'Doe', 'john.doe@mail.com', sha1('johndoe'));

INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (1000.00, 4, 'AT240000000015874963', 6);
INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (1000.00, 3, 'AT240950300077308934', 5);
INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (1000.00, 2, 'AT240003040087490150', 7);
INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (1000.00, 1, 'AT240009756201685150', 3);
INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (1000.00, 1, 'AT240009756957150150', 4);

INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES(7,4,'2020-10-20', 'Gehalt', 100);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES(6,3,'2020-10-20', 'Gewinnpraemie', 200);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES(3,4,'2020-10-20', 'Rueckerstattung', 300);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES(4,6,'2020-10-20', 'Gewinnausschuettung', 400);