<?php
session_start();

require_once "../models/User.php";
require_once "../models/Account.php";
require_once "../models/AccountTypes.php";
//error_reporting(0);
if (!$_SESSION['service']) {
    unset($_SESSION['id']);
    unset($_SESSION['login']);
    session_destroy();
    header("Location: ../index.html");
}

$accountTypes = AccountTypes::getAllAccounts();
$allAccounts = Account::showAllActiveAccounts();
?>
<html lang="de">
<head>
    <title>R24 SERVICE</title>
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link href="../css/styles.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/formstyle.css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="wrapper ">
        <div class="mt-5 container alert alert-info text-center">
            <h1>SERVICE-BEREICH</h1>
            <h2>Übersicht über R24-Konten</h2>
        </div>
        <br>
        <a href="logoutService.php" class="float-right btn btn-outline-info">[ Logout ]</a>
        <br>
    </div>
    <div class="card-body">
        <details>
            <summary>Liste aller registrierten Konten</summary>
            <div class="card-body">
                <div class="table-responsive table-striped">
                    <table class="table text-center">
                        <thead class="text-info">
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Land</th>
                        <th>Kontotyp</th>
                        <th>IBAN</th>
                        <th>Kontostand</th>
                        <th>Aktionen</th>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($allAccounts as $a) {
                                echo "<tr><td>" . $a->getULastName() . " " . $a->getUFirstName() . "</td>";
                                echo "<td>" . $a->getUMail() . "</td>";
                                echo "<td>" . $a->getULand() . "</td>";
                                echo "<td>" . $a->getTName() . "</td>";
                                echo "<td>" . $a->getAIBAN() . "</td>";
                                echo "<td> " . $a->getABalance() . "</td>";
                                echo "<td><a href='r24DetailsPage.php?id=".$a->getUID()."' class='btn btn-outline-info'>Details</a></td></tr>";
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </details>
    </div>
    <div class="card-body">
        <details>
            <summary>Liste aller verfügbaren Kontotypen</summary>
            <div class="card-body">
                <div class="table-responsive table-striped">
                    <table class="table text-center">
                        <thead class="text-info">
                        <th>Bezeichnung</th>
                        <th>Sollzinsen</th>
                        <th>Habenzinsen</th>
                        <th>Rate in EUR</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($accountTypes as $a) {
                            echo "<tr><td>" . $a->getTName() . "</td>";
                            echo "<td>" . $a->getTDebitInterest() . "%</td>";
                            echo "<td>" . $a->getTCreditInterest() . "%</td>";
                            echo "<td> " . $a->getTRate() . "</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </details>

    </div>
</div>
</body>
<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-left">Copyright © R24 Onlinebank 2020</div>
            <div class="col-lg-4 text-lg-right">
                <a class="mr-3" href="#">Privacy Policy</a>
                <a class="mr-3" href="#">Terms of Use</a>
                <a class="mr-3" href="installService.php">Reset Database</a>
            </div>
        </div>
    </div>
</footer>
</html>
