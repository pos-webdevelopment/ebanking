<!--
=========================================================
* Paper Dashboard 2 - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-2
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->

<?php
session_start();
require_once "../models/User.php";
require_once "../models/Account.php";
require_once "../models/Transaction.php";
if (!isset($_SESSION['login']) && isset($_SESSION['id']) || !$_SESSION['login']) {
    header("Location: ../index.html");
    exit();
} else {
    $ID = $_SESSION['id'];
}
$u = User::getUserById($ID);
$counter = 0;
if (isset($_POST['ref']) || isset($_POST['datumG']) || isset($_POST['datumZ1']) && isset($_POST['datumZ2'])) {
    if (!empty($_POST['ref'])) {
        $transaction = Transaction::searchReference($_POST['ref'], $ID);
    } else if (!empty($_POST['datumG'])) {
        $transaction = Transaction::searchExactDate($_POST['datumG'], $ID);
    } else if (!empty($_POST['datumZ1']) && !empty($_POST['datumZ2'])) {
        $transaction = Transaction::searchBetweenDates($_POST['datumZ1'], $_POST['datumZ2'], $ID);
    } else {
        $transaction = Transaction::getTransactionData($ID);
    }
} else {
    $transaction = Transaction::getTransactionData($ID);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>R24 - Deine Umsätze</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="../assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>

<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="warning">
        <div class="logo">
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-mini">
                <div class="logo-image-small">
                    <img src="../assets/img/login.png" alt="keyring">
                </div>
            </a>
            <a href="http://localhost/ebanking/index.html" class="simple-text logo-normal">Deine Bank.</a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="../lib/dashboard.php">
                        <i class="nc-icon nc-bank"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="profile.php">
                        <i class="nc-icon nc-single-02"></i>
                        <p>Mein R24-Account</p>
                    </a>
                </li>
                <li class="active ">
                    <a href="sales.php">
                        <i class="nc-icon nc-tile-56"></i>
                        <p>Umsätze</p>
                    </a>
                </li>
                <li>
                    <a href="transaktion.php">
                        <i class="nc-icon nc-bell-55"></i>
                        <p>Transaktionen</p>
                    </a>
                </li>
                <li>
                    <a href="../lib/upgrade.php">
                        <i class="nc-icon nc-spaceship"></i>
                        <p>Upgrade to PRO R24</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="javascript:">Umsätze</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <form>
                        <div class="input-group no-border">
                            <?php echo $u->getULastName() . ", " . $u->getUFirstName() ?>
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="nc-icon nc-bookmark-2"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">Navigation</span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="dashboard.php">Dashboard</a>
                                <a class="dropdown-item" href="transaktion.php">Umsätze</a>
                                <a class="dropdown-item" href="logoutService.php">Logout</a>
                            </div>
                        </li>
                        <li class="nav-item" title="Ausloggen">
                            <a class="nav-link btn-rotate" href="logoutService.php">
                                <i class="nc-icon nc-button-power"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="row">

            </div>
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                        <h4 class="card-title">Deine Umsätze</h4>
                        <p class="card-category">Zeige alle Umsätze auf Deinem R24 Konto an & behalte so einfach den
                            Überblick.</p>
                    </div>
                    <div>
                        <form method="post" action="sales.php" class="form">
                            <label for="ref">Nach Referenz suchen</label>
                            <input type="text" id="ref" name="ref" placeholder="zB. Miete"
                                   value="<?= isset($_POST['ref']) ? $_POST['ref'] : '' ?>" class="form-group"/>
                            <label for="datumG">| Nach Datum suchen</label>
                            <input type="date" id="datumG" name="datumG" placeholder="Nach Datum suchen"
                                   value="<?= isset($_POST['datumG']) ? $_POST['datumG'] : '' ?>" class="form-group"/>
                            <label for="datumZ1">| Datumbereich suchen: Von </label>
                            <input type="date" id="datumZ1" name="datumZ1"
                                   value="<?= isset($_POST['datumZ1']) ? $_POST['datumZ1'] : '' ?>" class="form-group"/>
                            <label for="datumZ2"> bis</label>
                            <input type="date" id="datumZ2" name="datumZ2"
                                   value="<?= isset($_POST['datumZ2']) ? $_POST['datumZ2'] : '' ?>" class="form-group"/>
                            <button class="btn btn-success" type="submit" value="submit">Suche starten</button>
                            <a href="sales.php" class="btn btn-warning">Filter leeren</a>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-info">
                                <th>Empfänger</th>
                                <th>Absender</th>
                                <th>Valuta</th>
                                <th>Verwendungszweck</th>
                                <th class="text-right">Betrag</th>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($transaction as $t) {
                                    $counter++;
                                    if ($t->getUPayer() == $ID) {
                                        $color = "alert alert-danger text-dark";
                                        $sign = "-";
                                    } else {
                                        $color = "alert alert-success text-dark";
                                        $sign = "+";
                                    }
                                    echo "<tr><td class='" . $color . "'>" . $t->Payee . "</td>";
                                    echo "<td class='" . $color . "'>" . $t->Payer . "</td>";
                                    echo "<td class='" . $color . "'>" . date("d.m.Y H:i:s", strtotime($t->getTDate())) . "</td>";
                                    echo "<td class='" . $color . "'>" . $t->getTReference() . "</td>";
                                    echo "<td class='text-right " . $color . "'>" . $sign . " EUR " . $t->getTAmount() . "</td></tr>";
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                            if($counter == 0) {
                                echo "<div class='text-center text-dark alert alert-warning'>";
                                echo "<h4 >Oje ... Leider wurden keine Einträge gefunden!</h4>";
                                echo "</div>";
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
            <button class="btn btn-info" onClick="window.print()">Seite ausdrucken</button>
        </div>
    </div>
    <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
            <div class="row">
                <nav class="footer-nav">
                    <ul>
                        <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                        <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                        <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
                    </ul>
                </nav>
                <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim
              </span>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>