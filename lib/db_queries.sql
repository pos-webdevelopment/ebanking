DELETE FROM tbl_user WHERE uID = 5;

# Show accounts
SELECT u.uLastname, u.uFirstName, u.uMail, u.uLand, t.tName, a.aIBAN, a.aBalance FROM tbl_account a
INNER JOIN tbl_account_types t ON a.tType = t.tID
INNER JOIN tbl_user u ON a.uID = u.uID;

SELECT uID FROM tbl_user WHERE uMail = "beton@fritz.at";

SELECT count(*) FROM tbl_transaction WHERE uPayee = 3 AND tDate > date_sub(now(),interval 1 day);
INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES(3,4,now(), 'Test', 100);

select date_sub(now(),interval 1 day);

SELECT * FROM tbl_account a
INNER JOIN tbl_account_types t ON a.tType = t.tID
INNER JOIN tbl_user u ON a.uID = u.uID
WHERE u.uID = 3;

SELECT * FROM tbl_user;
SELECT * FROM tbl_account_types;
SELECT * FROM tbl_account;
SELECT * FROM tbl_transaction;

SELECT *  FROM tbl_account_types;

SELECT concat(uVorname, ' ', uNachname) FROM tbl_user WHERE uID = 1;
SELECT aBalance FROM tbl_account WHERE uID = 1;

SELECT uPayee AS Begünstigter, u1.uLastname AS Begünstigter, uPayer AS Auftraggeber, u2.uLastname AS Auftraggeber, tAmount AS Betrag
FROM tbl_transaction t
INNER JOIN tbl_user u1 ON t.uPayee = u1.uID
INNER JOIN tbl_user u2 ON t.uPayer = u2.uID
#WHERE t.uPayer = 4
;

SELECT uId from tbl_user WHERE uMail = "franz@sepp.com";

INSERT INTO tbl_account(aBalance, tType, uID) VALUES (999, 1, 5);

SELECT uId FROM tbl_account WHERE aId = 4;

SELECT * FROM tbl_user;

UPDATE tbl_user SET uFirstname="Yamaha", uLastname="Motorcycles" WHERE uId = 1;
UPDATE tbl_user SET uFirstname="Maximilian", uLastname="Kohler" WHERE uId = 1;

SELECT uPass FROM tbl_user WHERE uMail = 'kohler@cern.ch';

SELECT * FROM tbl_user WHERE uFirstname = 'Ernst' AND uLastname = 'asdf' AND uMail = 'kohler@cern.com';

SELECT u.uLastname, t.uPayer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t INNER JOIN tbl_user u ON t.uPayee = u.uId
WHERE uPayee = 1 OR uPayer = 1;

SELECT concat(u1.uFirstName, " ", u1.uLastName) AS Payee, concat(u2.uFirstName, " ", u2.uLastName) AS Payer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t 
INNER JOIN tbl_user u1 ON t.uPayee = u1.uId
INNER JOIN tbl_user u2 ON t.uPayer = u2.uId;

# TRANSAKTION DURCHFÜHREN
SELECT * FROM tbl_transaction;
SELECT * FROM tbl_account;

INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES (1,4,'2020-12-31', 'Bezahlung Rechnung', 547.10);

UPDATE tbl_account a INNER JOIN tbl_transaction t ON a.uId = t.uPayee
SET a.aBalance = a.aBalance + 547.10 WHERE a.uId = 1; # Empfänger

UPDATE tbl_account a INNER JOIN tbl_transaction t ON a.uId = t.uPayer
SET a.aBalance = a.aBalance - 547.10 WHERE a.uId = 4; # Belasteter

UPDATE tbl_transaction SET tSuccess = 1 WHERE uPayee = 1 AND uPayer = 4 AND tDate = '2020-12-31' AND tAmount = 547.10;


SELECT * FROM tbl_account a INNER JOIN tbl_account_types t ON a.tType = t.tID INNER JOIN tbl_user u ON a.uID = u.uID WHERE u.uID = 6;
