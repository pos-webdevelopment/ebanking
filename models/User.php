<?php
require_once "Database.php";
class User
{
    private $uID = 0;
    private $uFirstName = '';
    private $uLastName = '';
    private $uMail = '';
    private $uPass = '';
    private $uStrasse = '';
    private $uOrt = '';
    private $uLand = '';
    private $uZIP = 0;
    private $uStatus = '';
    private $uProfilePic = '';

    /**
     * User constructor.
     */
    public function __construct(){}

    public static function getUserById($id)
    {
        $con = Database::connect();
        $sql = "SELECT * FROM tbl_user WHERE uId = ?";
        $query = $con->prepare($sql);
        $query->execute(array($id));
        $result = $query->fetchObject("User");
        Database::disconnect();
        return $result;
    }

    public static function updateUserData($id, $firstname, $lastname, $mail, $address, $city, $country, $zip, $status) {
        $con = Database::connect();
        $sql = "UPDATE tbl_user SET uFirstname=?, uLastname=?, uMail=?, uStrasse=?, uOrt=?, uLand=?, uZIP=?, uStatus=? WHERE uId = ?;";
        $query = $con->prepare($sql);
        $query->execute(array($firstname, $lastname, $mail, $address, $city, $country, $zip, $status, $id));
        Database::disconnect();
        if($query) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUserByMail($mail)
    {
        $con = Database::connect();
        $sql = "SELECT * FROM tbl_user WHERE uMail = ?";
        $query = $con->prepare($sql);
        $query->execute(array($mail));
        $result = $query->fetchObject("User");
        Database::disconnect();
        return $result;
    }
    # Helper function
    public static function getUserByName($name)
    {
        $split = explode(" ", $name);
        $firstName = $split[0];
        $lastName = $split[1];
        if ($split[0] != null && $split[1] != null && $split[2] == null) {
            $con = Database::connect();
            $sql = "SELECT uId FROM tbl_user WHERE uFirstName = ? AND uLastName = ?";
            $query = $con->prepare($sql);
            $query->execute(array($firstName, $lastName));
            $id = $query->fetchAll(PDO::FETCH_ASSOC);
            Database::disconnect();
            if($id) {
                return $id[0];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function login($mail, $pass)
    {
        $con = Database::connect();
        $sql = "SELECT uPass FROM tbl_user WHERE uMail = ?";
        $query = $con->prepare($sql);
        $query->execute(array($mail));
        $targetPass = $query->fetch();
        $pass = sha1($pass);
        Database::disconnect();
        if(strcmp($targetPass[0], $pass) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function register($firstname, $lastname, $mail, $pass) {
        $con = Database::connect();
        $sql = "SELECT * FROM tbl_user WHERE uFirstname = ? AND uLastname = ? AND uMail = ?;";
        $query = $con->prepare($sql);
        $query->execute(array($firstname, $lastname, $mail));
        $result = $query->fetchAll();
        Database::disconnect();
        if(!$result) {
            $sql = "INSERT INTO tbl_user(uFirstName, uLastName, uMail, uPass) VALUES(?, ?, ?, sha1(?));";
            $query = $con->prepare($sql);
            $query->execute(array($firstname, $lastname, $mail, $pass));
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public function getUId()
    {
        return $this->uID;
    }

    /**
     * @param int $uID
     */
    public function setUId($uID)
    {
        $this->uID = $uID;
    }

    /**
     * @return string
     */
    public function getUFirstName()
    {
        return $this->uFirstName;
    }

    /**
     * @param string $uFirstName
     */
    public function setUFirstName($uFirstName)
    {
        $this->uFirstName = $uFirstName;
    }

    /**
     * @return string
     */
    public function getULastName()
    {
        return $this->uLastName;
    }

    /**
     * @param string $uLastName
     */
    public function setULastName($uLastName)
    {
        $this->uLastName = $uLastName;
    }

    /**
     * @return string
     */
    public function getUMail()
    {
        return $this->uMail;
    }

    /**
     * @param string $uMail
     */
    public function setUMail($uMail)
    {
        $this->uMail = $uMail;
    }

    /**
     * @return string
     */
    public function getUPass()
    {
        return $this->uPass;
    }

    /**
     * @param string $uPass
     */
    public function setUPass($uPass)
    {
        $this->uPass = $uPass;
    }

    /**
     * @return string
     */
    public function getUStrasse()
    {
        return $this->uStrasse;
    }

    /**
     * @param string $uStrasse
     */
    public function setUStrasse($uStrasse)
    {
        $this->uStrasse = $uStrasse;
    }

    /**
     * @return string
     */
    public function getUOrt()
    {
        return $this->uOrt;
    }

    /**
     * @param string $uOrt
     */
    public function setUOrt($uOrt)
    {
        $this->uOrt = $uOrt;
    }

    /**
     * @return string
     */
    public function getUProfilePic()
    {
        return $this->uProfilePic;
    }

    /**
     * @param string $uProfilePic
     */
    public function setUProfilePic($uProfilePic)
    {
        $this->uProfilePic = $uProfilePic;
    }

    /**
     * @return string
     */
    public function getULand()
    {
        return $this->uLand;
    }

    /**
     * @param string $uLand
     */
    public function setULand($uLand)
    {
        $this->uLand = $uLand;
    }

    /**
     * @return int
     */
    public function getUZIP()
    {
        return $this->uZIP;
    }

    /**
     * @param int $uZIP
     */
    public function setUZIP($uZIP)
    {
        $this->uZIP = $uZIP;
    }

    /**
     * @return string
     */
    public function getUStatus()
    {
        return $this->uStatus;
    }

    /**
     * @param string $uStatus
     */
    public function setUStatus($uStatus)
    {
        $this->uStatus = $uStatus;
    }

}