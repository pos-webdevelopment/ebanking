<?php
require_once "Account.php";
require_once "User.php";
require_once "Database.php";

class Transaction
{
    private $tID = 0;
    private $uPayee = 0;
    private $uPayer = 0;
    private $tDate;
    private $tReference = '';
    private $tAmount = 0.00;
    private $tSuccess = false;

    # User Data
    private $uID = 0;
    private $uFirstName = '';
    private $uLastName = '';
    private $uLand = '';

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
    }

    public static function getTransactionData($id)
    {
        $con = Database::connect();
        $sql = "SELECT uPayer, uPayee, concat(u1.uFirstName, ' ', u1.uLastName) AS Payee, concat(u2.uFirstName, ' ', u2.uLastName) AS Payer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t
                INNER JOIN tbl_user u1 ON t.uPayee = u1.uId
                INNER JOIN tbl_user u2 ON t.uPayer = u2.uId
                WHERE uPayee = ? OR uPayer = ? ORDER BY tDate ASC;";
        $query = $con->prepare($sql);
        $query->execute(array($id, $id));
        $allTransactions = $query->fetchAll(PDO::FETCH_CLASS, "Transaction");
        Database::disconnect();
        $transactions = [];
        foreach ($allTransactions as $t) {
            $transactions[] = $t;
        }
        return $transactions;
    }


    # Main function for transaction
    public static function commit($payee, $payer, $reference, $iban, $amount, $date)
    {
        if (User::getUserByName($payee)['uId']) {
            $payee = User::getUserByName($payee)['uId']; # replace string with ID from payee
            if ($payee != 0 && Account::getAccountData($payee)->getAIBAN()==$iban) {
                $transaction = self::executeTransaction($payee, $payer, $date, $reference, $amount);
                $addBalance = Account::addBalanceToPayee($amount, $payee);
                $reduceBalance = Account::reduceBalanceFromPayer($amount, $payer);
                if ($transaction && $addBalance && $reduceBalance) {
                    if (self::setSuccessStatus($payee, $payer, $date, $amount)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function executeTransaction($payee, $payer, $date, $reference, $amount)
    {
        $con = Database::connect();
        $sql = "INSERT INTO tbl_transaction (uPayee, uPayer, tDate, tReference, tAmount) VALUES (?,?,?,?,?);";
        $query = $con->prepare($sql);
        $result = $query->execute(array($payee, $payer, $date, $reference, $amount));
        //$result = $query->fetchObject("Transaction");
        Database::disconnect();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    # Helper function
    private static function setSuccessStatus($payee, $payer, $date, $amount)
    {
        $con = Database::connect();
        $sql = "UPDATE tbl_transaction SET tSuccess = 1 WHERE uPayee = ? AND uPayer = ? AND tDate = ? AND tAmount = ?;";
        $query = $con->prepare($sql);
        $result = $query->execute(array($payee, $payer, $date, $amount));
        //$result = $query->fetchAll(PDO::FETCH_CLASS, "Transaction");
        Database::disconnect();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function currentTransactions($payee) {
        $con = Database::connect();
        $sql = "SELECT count(*) as total FROM tbl_transaction WHERE uPayee = ? AND tDate > date_sub(now(),interval 1 day);";
        $query = $con->prepare($sql);
        $query->execute(array($payee));
        return $query->fetchColumn();
    }

    public static function searchReference($ref, $id) {
        $con = Database::connect();
        $sql = "SELECT uPayer, uPayee, concat(u1.uFirstName, ' ', u1.uLastName) AS Payee, concat(u2.uFirstName, ' ', u2.uLastName) AS Payer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t
                INNER JOIN tbl_user u1 ON t.uPayee = u1.uId
                INNER JOIN tbl_user u2 ON t.uPayer = u2.uId
                WHERE tReference LIKE ? AND uPayee = ? OR uPayer = ?
                ORDER BY tDate ASC;";
        $query = $con->prepare($sql);
        $expr = "%".$ref."%";
        $query->execute(array($expr, $id, $id));
        $allTransactions = $query->fetchAll(PDO::FETCH_CLASS, "Transaction");
        Database::disconnect();
        $transactions = [];
        foreach ($allTransactions as $t) {
            $transactions[] = $t;
        }
        return $transactions;
    }

    public static function searchExactDate($d, $id) {
        $con = Database::connect();
        $sql = "SELECT uPayer, uPayee, concat(u1.uFirstName, ' ', u1.uLastName) AS Payee, concat(u2.uFirstName, ' ', u2.uLastName) AS Payer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t
                INNER JOIN tbl_user u1 ON t.uPayee = u1.uId
                INNER JOIN tbl_user u2 ON t.uPayer = u2.uId
                WHERE tDate LIKE ? AND uPayee = ? OR uPayer = ?
                ORDER BY tDate ASC;";
        $query = $con->prepare($sql);
        $expr = $d . "%";
        $query->execute(array($expr, $id, $id));
        $allTransactions = $query->fetchAll(PDO::FETCH_CLASS, "Transaction");
        Database::disconnect();
        $transactions = [];
        foreach ($allTransactions as $t) {
            $transactions[] = $t;
        }
        return $transactions;
    }

    public static function searchBetweenDates($d1, $d2, $id) {
        $con = Database::connect();
        $sql = "SELECT uPayer, uPayee, concat(u1.uFirstName, ' ', u1.uLastName) AS Payee, concat(u2.uFirstName, ' ', u2.uLastName) AS Payer, t.tDate, t.tReference, t.tAmount FROM tbl_transaction t
                INNER JOIN tbl_user u1 ON t.uPayee = u1.uId
                INNER JOIN tbl_user u2 ON t.uPayer = u2.uId
                WHERE tDate BETWEEN ? AND date_add(?,interval 1 day) AND uPayee = ? OR uPayer = ?
                ORDER BY tDate ASC;";
        $query = $con->prepare($sql);
        $query->execute(array($d1, $d2, $id, $id));
        $allTransactions = $query->fetchAll(PDO::FETCH_CLASS, "Transaction");
        Database::disconnect();
        $transactions = [];
        foreach ($allTransactions as $t) {
            $transactions[] = $t;
        }
        return $transactions;
    }

    /**
     * @return int
     */
    public function getTID()
    {
        return $this->tID;
    }

    /**
     * @param int $tID
     */
    public function setTID($tID)
    {
        $this->tID = $tID;
    }

    /**
     * @return int
     */
    public function getUPayee()
    {
        return $this->uPayee;
    }

    /**
     * @param int $uPayee
     */
    public function setUPayee($uPayee)
    {
        $this->uPayee = $uPayee;
    }

    /**
     * @return int
     */
    public function getUPayer()
    {
        return $this->uPayer;
    }

    /**
     * @param int $uPayer
     */
    public function setUPayer($uPayer)
    {
        $this->uPayer = $uPayer;
    }

    /**
     * @return mixed
     */
    public function getTDate()
    {
        return $this->tDate;
    }

    /**
     * @param mixed $tDate
     */
    public function setTDate($tDate)
    {
        $this->tDate = $tDate;
    }



    /**
     * @return string
     */
    public function getTReference()
    {
        return $this->tReference;
    }

    /**
     * @param string $tReference
     */
    public function setTReference($tReference)
    {
        $this->tReference = $tReference;
    }

    /**
     * @return float
     */
    public function getTAmount()
    {
        return $this->tAmount;
    }

    /**
     * @param float $tAmount
     */
    public function setTAmount($tAmount)
    {
        $this->tAmount = $tAmount;
    }

    /**
     * @return bool
     */
    public function isTSuccess()
    {
        return $this->tSuccess;
    }

    /**
     * @param bool $tSuccess
     */
    public function setTSuccess($tSuccess)
    {
        $this->tSuccess = $tSuccess;
    }

    /**
     * @return int
     */
    public function getUID()
    {
        return $this->uID;
    }

    /**
     * @param int $uID
     */
    public function setUID($uID)
    {
        $this->uID = $uID;
    }

    /**
     * @return string
     */
    public function getUFirstName()
    {
        return $this->uFirstName;
    }

    /**
     * @param string $uFirstName
     */
    public function setUFirstName($uFirstName)
    {
        $this->uFirstName = $uFirstName;
    }

    /**
     * @return string
     */
    public function getULastName()
    {
        return $this->uLastName;
    }

    /**
     * @param string $uLastName
     */
    public function setULastName($uLastName)
    {
        $this->uLastName = $uLastName;
    }

    /**
     * @return string
     */
    public function getULand()
    {
        return $this->uLand;
    }

    /**
     * @param string $uLand
     */
    public function setULand($uLand)
    {
        $this->uLand = $uLand;
    }


}