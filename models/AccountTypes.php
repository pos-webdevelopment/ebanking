<?php
require_once "Database.php";
class AccountTypes
{
    private $tID = 0;
    private $tName = '';
    private $tRate = 0.00;
    private $tDebitInterest = 0.00;
    private $tCreditInterest = 0.00;

    /**
     * AccountTypes constructor.
     */
    public function __construct()
    {
    }

    public static function getAllAccounts() {
        $con = Database::connect();
        $sql = "SELECT *  FROM tbl_account_types ORDER BY tName ASC;";
        $query = $con->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_CLASS, "AccountTypes");
        Database::disconnect();
        $accounts = [];
        foreach ($result as $a) {
            $accounts[] = $a;
        }
        return $accounts;
    }

    public static function getAccountDataById($id)
    {
        $con = Database::connect();
        $sql = "SELECT * FROM tbl_account_types WHERE tId = ?";
        $query = $con->prepare($sql);
        $query->execute(array($id));
        Database::disconnect();
        return $query->fetchObject("AccountTypes");
    }

    /**
     * @return int
     */
    public function getTID()
    {
        return $this->tID;
    }

    /**
     * @param int $tID
     */
    public function setTID($tID)
    {
        $this->tID = $tID;
    }

    /**
     * @return string
     */
    public function getTName()
    {
        return $this->tName;
    }

    /**
     * @param string $tName
     */
    public function setTName($tName)
    {
        $this->tName = $tName;
    }

    /**
     * @return float
     */
    public function getTRate()
    {
        return $this->tRate;
    }

    /**
     * @param float $tRate
     */
    public function setTRate($tRate)
    {
        $this->tRate = $tRate;
    }

    /**
     * @return float
     */
    public function getTDebitInterest()
    {
        return $this->tDebitInterest;
    }

    /**
     * @param float $tDebitInterest
     */
    public function setTDebitInterest($tDebitInterest)
    {
        $this->tDebitInterest = $tDebitInterest;
    }

    /**
     * @return float
     */
    public function getTCreditInterest()
    {
        return $this->tCreditInterest;
    }

    /**
     * @param float $tCreditInterest
     */
    public function setTCreditInterest($tCreditInterest)
    {
        $this->tCreditInterest = $tCreditInterest;
    }
}