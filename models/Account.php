<?php
require_once "Database.php";
class Account
{
    # Account Data
    private $aID = 0;
    private $aBalance = 0.00;
    private $tType = '';
    private $aIBAN = '';
    private $uID = 0;

    # User Data
    private $uFirstName = '';
    private $uLastName = '';
    private $uMail = '';
    private $uLand = '';

    # Account-Type Data
    private $tID = 0;
    private $tName = '';
    private $tRate = 0.00;
    private $tDebitInterest = 0.00;
    private $tCreditInterest = 0.00;


    /**
     * Account constructor.
     */
    public function __construct()
    {
    }

    public static function getAccountData($uID)
    {
        $con = Database::connect();
        $sql = "SELECT * FROM tbl_account a
                INNER JOIN tbl_account_types t ON a.tType = t.tID
                INNER JOIN tbl_user u ON a.uID = u.uID WHERE u.uID = ?";
        $query = $con->prepare($sql);
        $query->execute(array($uID));
        $result = $query->fetchObject("Account");
        Database::disconnect();
        return $result;
    }

    public static function createAccount($balance, $typeId, $iban, $userId) {
        $con = Database::connect();
        $sql = "INSERT INTO tbl_account(aBalance, tType, aIBAN, uID) VALUES (?, ?, ?, ?);";
        $query = $con->prepare($sql);
        $result =  $query->execute(array($balance, $typeId, $iban, $userId));
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    # Helper function
    public static function addBalanceToPayee($amount, $payee) {
        $con = Database::connect();
        $sql = "UPDATE tbl_account a INNER JOIN tbl_transaction t ON a.uId = t.uPayee
                SET a.aBalance = a.aBalance + ? WHERE a.uId = ?;";
        $query = $con->prepare($sql);
        $result = $query->execute(array($amount, $payee));
//        $result = $query->fetchAll(PDO::FETCH_CLASS, "Account");
        Database::disconnect();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    # Helper function
    public static function reduceBalanceFromPayer($amount, $payer) {
        $con = Database::connect();
        $sql = "UPDATE tbl_account a INNER JOIN tbl_transaction t ON a.uId = t.uPayer
                SET a.aBalance = a.aBalance - ? WHERE a.uId = ?;";
        $query = $con->prepare($sql);
        $result = $query->execute(array($amount, $payer));
        //$result = $query->fetchAll(PDO::FETCH_CLASS, "Account");
        Database::disconnect();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function showAllActiveAccounts() {
        $con = Database::connect();
        $sql = "SELECT a.aID, a.uID, u.uLastName, u.uFirstName, u.uMail, u.uLand, t.tName, a.aIBAN, a.aBalance FROM tbl_account a
                INNER JOIN tbl_account_types t ON a.tType = t.tID
                INNER JOIN tbl_user u ON a.uID = u.uID ORDER BY u.uLastName ASC;";
        $query = $con->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_CLASS, "Account");
        Database::disconnect();
        $accounts = [];
        foreach ($result as $a) {
            $accounts[] = $a;
        }
        return $accounts;
    }

    /**
     * @return int
     */
    public function getAID()
    {
        return $this->aID;
    }

    /**
     * @param int $aID
     */
    public function setAID($aID)
    {
        $this->aID = $aID;
    }

    /**
     * @return float
     */
    public function getABalance()
    {
        return $this->aBalance;
    }

    /**
     * @param float $aBalance
     */
    public function setABalance($aBalance)
    {
        $this->aBalance = $aBalance;
    }

    /**
     * @return string
     */
    public function getTType()
    {
        return $this->tType;
    }

    /**
     * @param string $tType
     */
    public function setTType($tType)
    {
        $this->tType = $tType;
    }

    /**
     * @return int
     */
    public function getUID()
    {
        return $this->uID;
    }

    /**
     * @param int $uID
     */
    public function setUID($uID)
    {
        $this->uID = $uID;
    }

    /**
     * @return string
     */
    public function getUFirstName()
    {
        return $this->uFirstName;
    }

    /**
     * @param string $uFirstName
     */
    public function setUFirstName($uFirstName)
    {
        $this->uFirstName = $uFirstName;
    }

    /**
     * @return string
     */
    public function getULastName()
    {
        return $this->uLastName;
    }

    /**
     * @param string $uLastName
     */
    public function setULastName($uLastName)
    {
        $this->uLastName = $uLastName;
    }

    /**
     * @return string
     */
    public function getUMail()
    {
        return $this->uMail;
    }

    /**
     * @param string $uMail
     */
    public function setUMail($uMail)
    {
        $this->uMail = $uMail;
    }

    /**
     * @return int
     */
    public function getTID()
    {
        return $this->tID;
    }

    /**
     * @param int $tID
     */
    public function setTID($tID)
    {
        $this->tID = $tID;
    }

    /**
     * @return string
     */
    public function getTName()
    {
        return $this->tName;
    }

    /**
     * @param string $tName
     */
    public function setTName($tName)
    {
        $this->tName = $tName;
    }

    /**
     * @return float
     */
    public function getTRate()
    {
        return $this->tRate;
    }

    /**
     * @param float $tRate
     */
    public function setTRate($tRate)
    {
        $this->tRate = $tRate;
    }

    /**
     * @return float
     */
    public function getTDebitInterest()
    {
        return $this->tDebitInterest;
    }

    /**
     * @param float $DebitInterest
     */
    public function setDebitInterest($DebitInterest)
    {
        $this->DebitInterest = $DebitInterest;
    }

    /**
     * @return float
     */
    public function getTCreditInterest()
    {
        return $this->tCreditInterest;
    }

    /**
     * @param float $CreditInterest
     */
    public function setCreditInterest($CreditInterest)
    {
        $this->CreditInterest = $CreditInterest;
    }

    /**
     * @return string
     */
    public function getAIBAN()
    {
        return $this->aIBAN;
    }

    /**
     * @param string $aIBAN
     */
    public function setAIBAN($aIBAN)
    {
        $this->aIBAN = $aIBAN;
    }

    /**
     * @return string
     */
    public function getULand()
    {
        return $this->uLand;
    }

    /**
     * @param string $uLand
     */
    public function setULand($uLand)
    {
        $this->uLand = $uLand;
    }
}